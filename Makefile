
all: dist package

dist:
	ck3modtool dist

package: dist
	ck3modtool package

clean:
	ck3modtool clean

.PHONY: dist package clean
