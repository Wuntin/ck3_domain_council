﻿domain_council_has_steward_trigger = {
	exists = cp:councillor_steward
}

domain_council_has_available_steward_trigger = {
	domain_council_has_steward_trigger = yes
	trigger_if = {
		limit = { domain_council_has_steward_trigger = yes }
		cp:councillor_steward = { is_available_for_activity_trigger = yes }
	}
}

# Needed for custom description.
domain_council_has_too_many_holdings_trigger = {
	domain_size > domain_limit
}

# Needed for custom description.
domain_council_has_too_many_held_duchies_trigger = {
	has_too_many_held_duchies_trigger = yes
}

domain_council_needed_trigger = {
	OR = {
		domain_council_has_too_many_holdings_trigger = yes
		domain_council_has_too_many_held_duchies_trigger = yes
	}
}

# Light-weight is_available, since is_available would fail on account
# of the person being in an activity (our activity).
domain_council_is_still_available_trigger = {
	is_alive = yes
	is_healthy = yes
	is_commanding_army = no
	is_imprisoned = no
}

domain_council_council_still_active_trigger = {
	# I have not closed the council
	NOT = { has_character_flag = domain_council_council_disbanded }

	# I still have too many titles
	domain_council_needed_trigger = yes

	# I am still available to participate
	domain_council_is_still_available_trigger = yes

	# My steward is still available to participate
	cp:councillor_steward = scope:my_steward
	scope:my_steward = {
		domain_council_is_still_available_trigger = yes
	}

	# My spouse is still available to participate (if invited)
	trigger_if = {
		limit = { exists = scope:my_spouse }

		primary_spouse = scope:my_spouse
		scope:my_spouse = {
			domain_council_is_still_available_trigger = yes
		}
	}
}
